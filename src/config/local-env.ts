namespace LocalEnv {
    export const HOST_1 = '/api';

    export const ACCEPTED_FILE_EXTENSIONS: readonly string[] = Object.freeze([
        '.jpg',
        '.jpeg',
        '.png',
        '.webp', //
    ]);

    export const MOBILE_MAX_WIDTH: number = 650;
}

export default LocalEnv;
