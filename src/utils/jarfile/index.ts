import _readFileAsBase64 from './read-file-as-base64';

namespace JarFile {
    export interface IBase64File {
        data: string;
        size: number;
        name: string;
        type: string;
    }

    export const readFileAsBase64 = _readFileAsBase64;
}

export default JarFile;
