import JarFile from '.';

async function readFileAsBase64(
    f: File,
    onProcessing?: (e: number) => any,
): Promise<JarFile.IBase64File> {
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();

        //? Fired when a read has completed successfully
        fileReader.addEventListener('load', (e) => {
            const result = e.target?.result;

            if (result === undefined || result === null) {
                return reject('Read file failed');
            }

            return resolve({
                data: btoa(result.toString()),
                name: f.name,
                size: f.size,
                type: f.type,
            });
        });

        //? Fired when the read failed due to an error
        fileReader.addEventListener('error', (e) => {
            return reject('Read file failed');
        });

        //? Fired periodically as data is read
        if (onProcessing) {
            fileReader.addEventListener('progress', (e) => {
                onProcessing(e.loaded / e.total);
                console.log();
            });
        }

        fileReader.readAsBinaryString(f);
    });
}

export default readFileAsBase64;
