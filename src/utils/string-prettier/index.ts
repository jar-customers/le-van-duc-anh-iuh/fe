namespace StringPrettier {
    const ONE_KB = 1024;
    const ONE_MB = ONE_KB * 1024;
    const ONE_GB = ONE_MB * 1024;
    export const binaryPrefix = (v?: number, fractionDigits: number = 2) => {
        if (!v) {
            return '';
        }

        // Byte
        if (v < ONE_KB) {
            return `${v} Byte`;
        }

        // KB
        if (v < ONE_MB) {
            return `${(v / ONE_KB).toFixed(fractionDigits)} KB`;
        }

        // MB
        if (v < ONE_GB) {
            return `${(v / ONE_MB).toFixed(fractionDigits)} MB`;
        }

        // > GB
        return `${(v / ONE_GB).toFixed(fractionDigits)} GB`;
    };
}

export default StringPrettier;
