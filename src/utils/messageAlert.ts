import { toast } from 'react-toastify';

const toastConfig = Object.freeze({
    position: 'top-right',
    autoClose: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: 'dark',
});

const messageAlert = (
    t: 'error' | 'success' | 'warning' | 'info' | 'default',
    mgs: string,
) => {
    if (t === 'default') {
        toast(mgs, toastConfig);
        return;
    }

    toast[t](mgs, toastConfig);
};

export default messageAlert;
