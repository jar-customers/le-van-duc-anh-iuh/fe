import JarStorage from '../jar-modules/jar-storage';

export interface StorageHistory {
    _id: string;
    threshold: number;
    createdAt: string;
    weightId: string;
}

interface StorageInit {
    history: StorageHistory[];
}

class StorageClient extends JarStorage<StorageInit> {
    public constructor() {
        super({
            storageKey: 'yolov7',
            initStore: {
                history: [],
            },
        });
    }

    public get history() {
        return this._data.history;
    }

    public set history(v: StorageHistory[]) {
        this._data.history = [...v];
        this.save();
    }
}

const storageClient = new StorageClient();
export default storageClient;
