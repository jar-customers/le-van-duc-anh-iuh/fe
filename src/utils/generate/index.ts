import generateId from './generateId';

const generate = Object.freeze({
    id: generateId,
});

export default generate as Readonly<typeof generate>;
