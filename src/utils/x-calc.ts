namespace XCalc {
    export const imageSize = (uri: string): Promise<[number, number]> => {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                resolve([img.width, img.height]);
            };
            img.src = uri;
        });
    };
}

export default XCalc;
