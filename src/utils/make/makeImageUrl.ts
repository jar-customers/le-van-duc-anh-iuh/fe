import LocalEnv from '../../config/local-env';

const makeImageUrl = (name: string, fileNameExtension: string = '') => {
    return `${LocalEnv.HOST_1}/static/images/${name}${fileNameExtension}`;
};

export default makeImageUrl;
