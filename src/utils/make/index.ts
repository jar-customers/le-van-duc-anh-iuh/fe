import makeClassName from './className';
import makeImageUrl from './makeImageUrl';
import makeArray from './makeArray';
import makeResult from './makeResult';

const make = Object.freeze({
    imageUrl: makeImageUrl,
    result: makeResult,
    array: makeArray,
    className: makeClassName,
});

export default make;
