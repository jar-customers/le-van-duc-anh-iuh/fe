abstract class JarStorage<T> {
    protected _storageKey: string;
    protected _data: T;

    protected constructor(props: { storageKey: string; initStore: T }) {
        this._storageKey = props.storageKey;
        this._data = {
            ...props.initStore,
        };
        this.load();
    }

    public load = () => {
        const strData = localStorage.getItem(this._storageKey);

        if (!strData) {
            this.save();
            return;
        }

        try {
            const rawData = JSON.parse(strData);
            this._data = rawData;
            return;
        } catch {
            this.save();
            return;
        }
    };

    public save = () => {
        if (!this._data) {
            console.warn('WARNING: null or undefined was valued of YarStorage');
            return;
        }
        localStorage.setItem(this._storageKey, JSON.stringify(this._data));
    };
}

export default JarStorage;
