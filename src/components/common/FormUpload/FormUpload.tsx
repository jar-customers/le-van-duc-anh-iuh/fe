import * as React from 'react';
import { FormUploadSelectedEvent } from '.';
import LocalEnv from '../../../config/local-env';
import useGlobalStore from '../../../hooks/useGlobalStore';
import { Layer, LayerContainer } from '../../../jar-components/jar-layer';
import JarFile from '../../../utils/jarfile';
import make from '../../../utils/make';
import messageAlert from '../../../utils/messageAlert';
import Container from '../Container';
import styles from './FormUpload.module.scss';

export interface IFormUploadProps {
    onSelected?: FormUploadSelectedEvent;
}

export default function FormUpload(props: IFormUploadProps) {
    const { onSelected } = props;

    const { isDragging } = useGlobalStore();

    const inputRef = React.useRef<HTMLInputElement>(null);

    const handleInputChange = React.useCallback(async () => {
        if (!inputRef.current) {
            messageAlert('error', 'Input chưa sẵn sàng !');
            return;
        }

        if (!inputRef.current.files || inputRef.current.files.length === 0) {
            return messageAlert('warning', 'Bạn chưa chọn file nào !');
        }

        const file = await JarFile.readFileAsBase64(inputRef.current.files[0]);
        if (onSelected) {
            onSelected({
                value: file,
            });
        }

        inputRef.current.value = '';
    }, [onSelected]);

    return (
        <div
            className={make.className([
                styles['form-upload'],
                isDragging && styles['is-dragging'],
            ])}
        >
            <Container>
                <div className={styles['wrap']}>
                    <LayerContainer className={styles['layer-container']}>
                        <Layer index={0} className={styles['label-layer']}>
                            <i>Chọn, hoặc kéo thả ảnh vào đây</i>
                        </Layer>
                        <Layer index={1} className={styles['input-layer']}>
                            <input
                                type={'file'}
                                accept={LocalEnv.ACCEPTED_FILE_EXTENSIONS.join(', ')}
                                ref={inputRef}
                                onChange={handleInputChange}
                            />
                        </Layer>
                    </LayerContainer>
                </div>
            </Container>
        </div>
    );
}
