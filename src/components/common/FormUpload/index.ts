import JarFile from '../../../utils/jarfile';
import FormUpload from './FormUpload';

export type FormUploadSelectedEvent = (e: { value: JarFile.IBase64File }) => any;

export default FormUpload;
