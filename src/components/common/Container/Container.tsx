import * as React from 'react';
import CommonTypes from '../../../types/common-types';
import make from '../../../utils/make';
import styles from './Container.module.scss';

export interface IContainerProps extends CommonTypes.DefaultReactProps {}

export default function Container(props: IContainerProps) {
    return (
        <div className={make.className([styles['container'], props.className])}>
            {props.children}
        </div>
    );
}
