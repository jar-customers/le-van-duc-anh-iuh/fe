import * as React from 'react';
import { Layer, LayerContainer } from '../../../jar-components/jar-layer';
import CommonTypes from '../../../types/common-types';
import styles from './Loading.module.scss';

export interface ILoadingProps extends CommonTypes.DefaultReactProps {}

export default function Loading(props: ILoadingProps) {
    return (
        <div className={styles['loading']}>
            <LayerContainer className={styles['layer-container']}>
                <Layer index={0}>
                    <div className={styles['spin']} />
                </Layer>
                <Layer index={1}>
                    <div className={styles['content-container']}>{props.children}</div>
                </Layer>
            </LayerContainer>
        </div>
    );
}
