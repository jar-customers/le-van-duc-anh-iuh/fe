import * as React from 'react';

import BBox from './BBox';
import styles from './ImagePredicted.module.scss';
import CommonTypes from '../../../types/common-types';
import make from '../../../utils/make';
import { Layer, LayerContainer } from '../../../jar-components/jar-layer';
import Predict from '../../../api/predict';

export interface IImagePredictedProps extends CommonTypes.DefaultReactProps {
    data: Predict.PredictResponse;
}

export default function ImagePredicted(props: IImagePredictedProps) {
    // const aspectRatio = props.data.predict.aspectRatio;
    const { _id, items, originalSize } = props.data;

    const aspectRatio = originalSize.width / originalSize.height;

    const bboxElements = React.useMemo(() => {
        return items.map((item, i) => {
            const labels: string[] = item.labels.map((v) => v.name);
            return (
                <Layer index={i + 1} key={item._id}>
                    <BBox
                        top={item.top}
                        left={item.left}
                        width={item.width}
                        height={item.height}
                        labels={labels}
                    />
                </Layer>
            );
        });
    }, [items]);

    return (
        <div
            className={make.className([styles['image-predicted'], props.className])}
            style={
                {
                    '--aspect-ratio': aspectRatio,
                } as CommonTypes.CSSProperties
            }
        >
            <LayerContainer className={styles['layer-container']}>
                <Layer
                    className={styles['layer-image']}
                    index={0}
                    style={
                        {
                            backgroundImage: `url('${make.imageUrl(_id, '.webp')}')`,
                        } as CommonTypes.CSSProperties
                    }
                />
                {bboxElements}
            </LayerContainer>
        </div>
    );
}
