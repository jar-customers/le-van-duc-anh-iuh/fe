import * as React from 'react';
import CommonTypes from '../../../../types/common-types';
import make from '../../../../utils/make';

import styles from './BBox.module.scss';

export interface IBBoxProps extends CommonTypes.DefaultReactProps {
    top: number;
    left: number;
    width: number;
    height: number;
    labels: string[];
    borderColor?: string;
}

export default function BBox(props: IBBoxProps) {
    return (
        <div
            className={make.className([styles['bbox'], props.className])}
            style={{
                top: `${props.top * 100}%`,
                left: `${props.left * 100}%`,
                width: `${props.width * 100}%`,
                height: `${props.height * 100}%`,
                borderColor: props.borderColor || 'rgb(0,255,255)',
            }}
        >
            <ul>
                {props.labels.map((item, i) => {
                    return <li key={i}>{item}</li>;
                })}
            </ul>
        </div>
    );
}
