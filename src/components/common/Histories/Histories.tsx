import * as React from 'react';
import { NavLink } from 'react-router-dom';

import useGlobalStore from '../../../hooks/useGlobalStore';
import CommonTypes from '../../../types/common-types';
import make from '../../../utils/make';

import Control from './Control';
import styles from './Histories.module.scss';

export interface IHistoriesProps extends CommonTypes.DefaultReactProps {}

export default function Histories(props: IHistoriesProps) {
    const { historyItems } = useGlobalStore();

    const [keyword, setKeyword] = React.useState<string>('');

    const handleSearch = React.useCallback((v: string) => {
        setKeyword(v);
    }, []);

    const liElements = React.useMemo(() => {
        const remainingHistoryItems = make.result(() => {
            if (!keyword) return historyItems;

            const keywordRegEx = new RegExp(`${keyword}`);

            return historyItems.filter((v) => {
                return keywordRegEx.test(v.keyword);
            });
        });

        return remainingHistoryItems.map((item) => {
            return (
                <li key={item._id} className={styles['item']}>
                    <NavLink
                        to={`/predicted/${item.predictId}`}
                        className={({ isActive }) =>
                            isActive ? styles['is-active'] : undefined
                        }
                    >
                        <strong>{item.name}</strong>
                        <span>{item.createdAt.toLocaleString()}</span>
                    </NavLink>
                </li>
            );
        });
    }, [historyItems, keyword]);

    return (
        <div className={make.className([styles['histories'], props.className])}>
            <Control onSearch={handleSearch} />

            <ul>{liElements}</ul>
        </div>
    );
}
