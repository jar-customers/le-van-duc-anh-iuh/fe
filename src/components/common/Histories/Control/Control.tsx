import * as React from 'react';
import styles from './Control.module.scss';
import { useDebouncedCallback } from 'use-debounce';
import removeAccents from 'remove-accents';

export interface IControlProps {
    onSearch?: (v: string) => any;
}

export default function Control(props: IControlProps) {
    const { onSearch } = props;

    const debounced = useDebouncedCallback<(v: string) => any>((v) => {
        if (onSearch) {
            onSearch(removeAccents.remove(v).replace(/\s+/g, ' ').trim().toLowerCase());
        }
    }, 400);

    return (
        <div className={styles['control']}>
            <div className={styles['input-name-wrap']}>
                <input
                    className={styles['input-name']}
                    type={'text'}
                    placeholder='Tìm kiếm theo tên'
                    onChange={(e) => {
                        debounced(e.currentTarget.value);
                    }}
                />
            </div>
        </div>
    );
}
