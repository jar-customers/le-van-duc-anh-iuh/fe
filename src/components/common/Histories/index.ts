import Histories from './Histories';

export interface HistoryItem {
    _id: string;
    predictId: string;
    name: string;
    createdAt: Date;
}

export default Histories;
