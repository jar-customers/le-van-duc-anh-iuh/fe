import * as React from 'react';
import styles from './Header.module.scss';
import { Link } from 'react-router-dom';
import useGlobalStore from '../../../hooks/useGlobalStore';

export interface IHeaderProps {}

export default function Header(props: IHeaderProps) {
    const { appName } = useGlobalStore();

    return (
        <header className={styles['header']}>
            <Link to={'/'} className={styles['title']}>
                {appName}
            </Link>
        </header>
    );
}
