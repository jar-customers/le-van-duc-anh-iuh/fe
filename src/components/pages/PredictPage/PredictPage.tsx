import * as React from 'react';
import styles from './PredictPage.module.scss';
import { useParams } from 'react-router-dom';
import api from '../../../api';
import Predict from '../../../api/predict';
import messageAlert from '../../../utils/messageAlert';
import ImagePredicted from '../../common/ImagePredicted';
import Container from '../../common/Container';

export interface IPredictPageProps {}

export default function PredictPage(props: IPredictPageProps) {
    const { id } = useParams();

    const [predictedInfo, setPredictedInfo] = React.useState<
        Predict.PredictResponse | undefined
    >(undefined);
    const loadPredictedInfo = React.useCallback(async (_id: string) => {
        messageAlert('info', 'Đang lấy thông tin kết quả dự đoán !');
        const res = await api.Predict.findOne(_id);

        if (!res) {
            return messageAlert('error', 'Lấy dữ thông tin dự đoán thất bại !');
        }

        messageAlert('success', 'Lấy thông tin kết quả dự đoán thành công');
        setPredictedInfo(res);
    }, []);

    React.useEffect(() => {
        if (!id) {
            messageAlert('error', 'Url không hợp lệ !');
            return;
        }

        loadPredictedInfo(id);
    }, [id, loadPredictedInfo]);

    return (
        <div className={styles['predict-page']}>
            <Container>
                {!predictedInfo ? null : (
                    <ImagePredicted
                        data={predictedInfo}
                        className={styles['image-predicted']}
                    />
                )}
            </Container>
        </div>
    );
}
