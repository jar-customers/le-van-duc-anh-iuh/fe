import * as React from 'react';
import api from '../../../api';
import messageAlert from '../../../utils/messageAlert';
import FormUpload, { FormUploadSelectedEvent } from '../../common/FormUpload';
import styles from './HomePage.module.scss';
import { useNavigate } from 'react-router-dom';
// import useGlobalStore from '../../../hooks/useGlobalStore';
import Loading from '../../common/Loading';

export interface IHomePageProps {}

export default function HomePage(props: IHomePageProps) {
    const [isLoading, setIsLoadig] = React.useState<boolean>(false);
    const navigate = useNavigate();
    // const { loadHistories } = useGlobalStore();

    const handleSelected: FormUploadSelectedEvent = React.useCallback(
        async (e) => {
            setIsLoadig(true);
            messageAlert('info', 'Đang tải lên và xử lý kết quả...');
            const res = await api.Predict.predict({
                file: e.value,
            });

            if (!res) {
                return messageAlert('error', 'Có lỗi trong quá trình tải lên !');
            }

            messageAlert('success', 'Thành công.');
            // await loadHistories();

            setIsLoadig(false);

            navigate(`/predicted/${res._id}`);
        },
        [navigate],
    );

    return (
        <div className={styles['home-page']}>
            {isLoading ? (
                <div className={styles['loading-container']}>
                    <Loading>Đang tải lên và xử lý kết quả</Loading>
                </div>
            ) : (
                <FormUpload onSelected={handleSelected} />
            )}
        </div>
    );
}
