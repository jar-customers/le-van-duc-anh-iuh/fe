import * as React from 'react';
import CommonTypes from '../../../types/common-types';
import styles from './DefaultLayout.module.scss';
import { Outlet } from 'react-router-dom';
import Header from '../../common/Header';
import Histories from '../../common/Histories';

export interface IDefaultLayoutProps extends CommonTypes.DefaultReactProps {}

export default function DefaultLayout(props: IDefaultLayoutProps) {
    return (
        <div className={styles['default-layout']}>
            <Header />
            <main className={styles['main']}>
                <div className={styles['histories-container']}>
                    <Histories />
                </div>
                <div className={styles['main-container']}>
                    <Outlet />
                </div>
            </main>
        </div>
    );
}
