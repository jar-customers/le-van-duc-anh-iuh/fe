import * as React from 'react';
import api from '../api';
import Predict from '../api/predict';
import LocalEnv from '../config/local-env';
import HistoryItem from '../types/HistoryItem';
import messageAlert from '../utils/messageAlert';
import removeAccents from 'remove-accents';
import make from '../utils/make';

export type TDeviceType = 'mobile' | 'tablet' | 'pc';

export interface IGlobalProviderProps {}

export const globalContext = React.createContext<{
    deviceType: TDeviceType;
    isDragging: boolean;
    historyItems: HistoryItem[];
    loadHistories: () => Promise<void>;
    appName: string;
}>({} as any);

export function GlobalProvider(props: React.PropsWithChildren<IGlobalProviderProps>) {
    const timerRef = React.useRef<NodeJS.Timer | null>(null);
    const counterRef = React.useRef<number>(0);

    const [items, setItems] = React.useState<Predict.PredictResponse[]>([]);
    const [deviceType, setDeviceType] = React.useState<TDeviceType>('pc');
    const [isDragging, setIsDragging] = React.useState<boolean>(false);
    const [appName, setAppName] = React.useState<string>('Tên ứng dụng');

    const deviceTypeRef = React.useRef<TDeviceType>(deviceType);
    deviceTypeRef.current = deviceType;

    const loadHistories = React.useCallback(async () => {
        const res = await api.Predict.listAll();
        if (!res) {
            messageAlert('error', 'Load lịch sử thất bại');
            return;
        }

        res.data.sort((a, b) => {
            const aCreatedAt = a.createdAt;
            const bCreatedAt = b.createdAt;

            if (aCreatedAt < bCreatedAt) return 1;
            if (aCreatedAt > bCreatedAt) return -1;
            return 0;
        });

        setItems(res.data);
    }, []);

    const historyItems: HistoryItem[] = React.useMemo(() => {
        return items.reduce((sum, item) => {
            item.items.forEach((v) => {
                const name = v.labels[0].name || 'undefined';
                sum.push({
                    _id: v._id,
                    predictId: item._id,
                    name: name,
                    createdAt: new Date(item.createdAt),
                    keyword: removeAccents
                        .remove(name)
                        .replace(/\s+/, ' ')
                        .trim()
                        .toLowerCase(),
                });
            });
            return sum;
        }, [] as HistoryItem[]);
    }, [items]);

    const updateDeviceType = React.useCallback((v: TDeviceType) => {
        if (deviceTypeRef.current === v) {
            return;
        }
        setDeviceType(v);
    }, []);

    const handleDocumentResize = React.useCallback(() => {
        const w = window.innerWidth;
        const h = window.innerHeight;

        if (w < LocalEnv.MOBILE_MAX_WIDTH) {
            updateDeviceType('mobile');
        }

        const aspectRatio = w / h;

        if (aspectRatio >= 1) {
            updateDeviceType('pc');
            return;
        }

        updateDeviceType('tablet');
    }, [updateDeviceType]);

    const handleDragging = React.useCallback(() => {
        counterRef.current = 5;

        if (timerRef.current) return;

        // drag start
        setIsDragging(true);
        //

        timerRef.current = setInterval(() => {
            if (counterRef.current <= 0) {
                if (!timerRef.current) {
                    console.error('clear interval failed');
                    return;
                }

                clearInterval(timerRef.current);
                timerRef.current = null;

                // drag end
                setIsDragging(false);
                //
                return;
            }

            counterRef.current -= 1;
        }, 100);
    }, []);

    React.useEffect(() => {
        window.addEventListener('resize', handleDocumentResize);
        window.addEventListener('dragover', handleDragging);
        return () => {
            window.removeEventListener('resize', handleDocumentResize);
            window.removeEventListener('dragover', handleDragging);
        };
    }, [handleDocumentResize, handleDragging]);

    // Load histories
    React.useEffect(() => {
        loadHistories();
    }, [loadHistories]);

    React.useEffect(() => {
        make.result(async () => {
            const res = await api.appName();
            const newTitle = res || 'Tên ứng dụng';
            setAppName(newTitle);
            document.title = newTitle;
        });
    }, []);

    return (
        <globalContext.Provider
            value={{
                appName,
                deviceType, //
                isDragging, //
                historyItems,
                loadHistories,
            }}
        >
            {props.children}
        </globalContext.Provider>
    );
}
