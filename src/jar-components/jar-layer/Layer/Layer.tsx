import * as React from 'react';
import CommonTypes from '../../../types/common-types';
import make from '../../../utils/make';

import styles from './Layer.module.scss';

export type TLayerProps = React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
> & {
    style?: CommonTypes.CSSProperties;
    index: number;
};

export default function Layer(props: TLayerProps) {
    const divProps = {
        ...props,
        // index: undefined,
        style: undefined,
        className: undefined,
    };

    const style = props.style || {};
    style.top = `-${props.index * 100}%`;

    // console.log(style);

    return (
        <div
            {...divProps}
            className={make.className([styles['layer'], props.className])}
            style={style}
        />
    );
}
