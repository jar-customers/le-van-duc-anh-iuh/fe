import * as React from 'react';
import make from '../../../utils/make';
import styles from './LayerContainer.module.scss';

export default function LayerContainer(
    props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
) {
    return (
        <div
            {...props}
            className={make.className([styles['layer-container'], props.className])}
        />
    );
}
