interface HistoryItem {
    _id: string;
    predictId: string;
    name: string;
    createdAt: Date;
    keyword: string;
}

export default HistoryItem;
