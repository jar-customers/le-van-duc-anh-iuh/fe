import React from 'react';

declare module CommonTypes {
    export type CSSProperties = React.CSSProperties & {
        [k: string]: string | number;
    };

    export interface DefaultReactProps {
        children?: React.ReactNode;
        className?: string;
        style?: CSSProperties;
    }
}

export default CommonTypes;
