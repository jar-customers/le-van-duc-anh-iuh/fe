import * as React from 'react';
import { globalContext } from '../store/global-store';

const useGlobalStore = () => {
    return React.useContext(globalContext);
};

export default useGlobalStore;
