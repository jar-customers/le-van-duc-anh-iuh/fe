import axios from 'axios';
import LocalEnv from '../config/local-env';

export const appName = async (): Promise<string | undefined> => {
    try {
        const res = await axios.get(`${LocalEnv.HOST_1}/`);
        return res.data;
    } catch {
        return undefined;
    }
};
