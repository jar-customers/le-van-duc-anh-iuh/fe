import axios from 'axios';
import LocalEnv from '../config/local-env';
import JarFile from '../utils/jarfile';

namespace Predict {
    export declare module PredictedResult {
        export interface Label {
            digital: number;
            name: string;
        }

        export interface Item {
            _id: string;
            height: number;
            labels: Label[];
            left: number;
            top: number;
            width: number;
        }

        export interface OriginalSize {
            height: number;
            width: number;
        }

        export interface RootObject {
            _id: string;
            items: Item[];
            originalSize: OriginalSize;
            createdAt: string;
        }
    }

    export type PredictFormData = {
        file: JarFile.IBase64File;
    };

    export type PredictResponse = PredictedResult.RootObject;

    const predictUrl = `${LocalEnv.HOST_1}/predict`;
    export const predict = async (
        formData: PredictFormData,
    ): Promise<PredictResponse | undefined> => {
        try {
            const res = await axios.post(predictUrl, formData);
            return res.data;
        } catch {
            return undefined;
        }
    };

    export const findOne = async (id: string): Promise<PredictResponse | undefined> => {
        try {
            const res = await axios.get([LocalEnv.HOST_1, 'find-one'].join('/'), {
                params: {
                    id,
                },
            });
            return res.data;
        } catch {
            return undefined;
        }
    };

    export type FindAllResponse = {
        data: PredictedResult.RootObject[];
    };

    const listAllUrl = `${LocalEnv.HOST_1}/list-all`;
    export const listAll = async (): Promise<FindAllResponse | undefined> => {
        try {
            const res = await axios.get(listAllUrl);
            return res.data;
        } catch {
            return undefined;
        }
    };
}

export default Predict;
