import { appName } from './appName';
import Predict from './predict';

const api = Object.freeze({
    appName,
    Predict,
});

export default api;
