import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Routes, Route } from 'react-router-dom';

import styles from './App.module.scss';
import DefaultLayout from '../components/layouts/DefaultLayout';
import HomePage from '../components/pages/HomePage';
import { GlobalProvider } from '../store/global-store';
import PredictPage from '../components/pages/PredictPage';

function App() {
    return (
        <div className={styles['app']}>
            <GlobalProvider>
                <Routes>
                    <Route path='/' element={<DefaultLayout />}>
                        <Route index element={<HomePage />} />
                        <Route path='/predicted/:id' element={<PredictPage />} />
                    </Route>
                </Routes>
            </GlobalProvider>
            <ToastContainer />
        </div>
    );
}

export default App;
